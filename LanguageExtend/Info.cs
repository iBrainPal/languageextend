﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZE
{
    
    public class Info
    {
        public enum Kinds { Text, Message, Notification, Alert, Warning, Error }

        public Kinds Kind { get; set; }
        public string Code { get; set; }
        public string Text { get; set; }

        public Info(Kinds kind, string code)
        {
            Construct(kind, code, string.Empty);
        }

        public Info(Kinds kind, string code, string text)
        {
            Construct(kind, code, text);
        }

        private void Construct(Kinds kind, string code, string text)
        {
            this.Kind = kind;
            this.Code = code;
            this.Text = text;
        }
    }
}
