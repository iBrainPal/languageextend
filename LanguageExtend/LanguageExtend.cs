﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ZE
{
    [ProvideProperty("LanguageID", typeof(Control))]
    public class LanguageExtend : Component, IExtenderProvider
    {
        public delegate void OnLanguageChanged();
        public event OnLanguageChanged LanguageChanged;

        public enum Kinds { None, English, China, Japan }
        
        private Hashtable _languageID = new Hashtable();

        private System.ComponentModel.Container components = null;

        public LanguageExtend(System.ComponentModel.IContainer container)
        {
            InitializeComponent();
        }
        

        [DefaultValue(""), Description("Language ID")]
        public string GetLanguageID(object control)
        {
            string reValue = string.Empty;
            if (this._languageID.Contains(control))
            {
                reValue = this._languageID[control].ToString();
            }

            return reValue;
        }
        public void SetLanguageID(object control, string value)
        {
            if (this._languageID.Contains(control))
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this._languageID[control] = value;
                }
                else
                {
                    this._languageID.Remove(control);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this._languageID.Add(control, value);

                }
            }
        }

        private Kinds defaultLanguage = Kinds.None;
        [Browsable(false)]
        public Kinds CurrentLanguage { get { return defaultLanguage; } }

        
        [Browsable(false)]
        public bool CancelWhenNotFind { get; set; }

        private Kinds GetCurrentLanguage()
        {
            Kinds currentLanguage = Kinds.None;

            if (CultureInfo.InstalledUICulture.Name.StartsWith("zh"))
            {
                currentLanguage = Kinds.China;
            }
            else if (CultureInfo.InstalledUICulture.Name.StartsWith("ja"))
            {
                currentLanguage = Kinds.Japan;
            }
            else if (CultureInfo.InstalledUICulture.Name.StartsWith("en"))
            {
                currentLanguage = Kinds.English;
            }

            return currentLanguage;
        }
        

        public void LoadDefaultLanguage(Config config)
        {
            LoadLanguage(GetCurrentLanguage(), config);
        }

        public void LoadLanguage(Kinds language, Config config)
        {
            if (defaultLanguage != language)
            {
                defaultLanguage = language;

                foreach (object obj in _languageID.Keys)
                {
                    string text = config.ReadValue(language.ToString(), _languageID[obj].ToString());

                    if (!CancelWhenNotFind || !string.IsNullOrEmpty(text))
                    {
                        InvokeSetText(obj, text);
                    }
                }

                if (this.LanguageChanged != null && this.LanguageChanged.GetInvocationList().Length > 0)
                {
                    LanguageChanged();
                }
            }
        }

        #region Change Text By Invoke

        private delegate void SetControlText(object obj, string text);
        private SetControlText setControlText;

        private void SetText(object obj, string text)
        {
            //control.Text = text;
            if (obj is Info)
            {
                ((Info)obj).Text = text;
            }
            else if (obj is Control)
            {
                ((Control)obj).Text = text;
            }
            else if (obj is ToolStripMenuItem)
            {
                ((ToolStripMenuItem)obj).Text = text;
            }
        }

        private void InvokeSetText(object obj, string text)
        {
            if (obj is Info || obj is ToolStripMenuItem)
            {
                SetText(obj, text);
            }
            else if (obj is Control)
            {
                //((Control)obj).Text = text;
                Control control = (Control)obj;

                if (control.InvokeRequired)
                {
                    control.Invoke(setControlText, control, text);
                }
                else
                {
                    SetText(control, text);
                }
            }
        }

        #endregion

        public LanguageExtend()
        {
            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码
        /**/
        /// 
        private void InitializeComponent()
        {
            CancelWhenNotFind = true;
            components = new System.ComponentModel.Container();
            setControlText = new SetControlText(SetText);
        }
        #endregion

        #region IExtenderProvider 成员
        public bool CanExtend(object extendee)
        {
            // TODO: 添加 Component1.CanExtend 实现
            if ((extendee is Control || extendee is Info || extendee is ToolStripMenuItem) && !(extendee is LanguageExtend))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

    }


}
