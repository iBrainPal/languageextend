﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace ZE
{
    public class Config
    {
        private string inipath;
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, byte[] val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, byte[] retVal, int size, string filePath);
        
        private Encoding encoding = Encoding.UTF8;
        public Encoding CharEncoding { get { return encoding; } set { encoding = value; } }

        public Config(string filename)
        {
            inipath = filename;
        }

        public Config(string filename, Encoding charEncoding)
        {
            this.inipath = filename;
            this.encoding = charEncoding;
        }
        
        public void WriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, encoding.GetBytes(Value), this.inipath);
        }

        public string ReadValue(string Section, string Key)
        {
            byte[] temp = new byte[512];
            int i = 0;
            if (!File.Exists(this.inipath))
            {
                WriteValue(Section, Key, string.Empty);
            }

            i = GetPrivateProfileString(Section, Key, "", temp, 512, this.inipath);

            return encoding.GetString(temp, 0, i);
        }

        public int ReadIntValue(string section, string key)
        {
            int reValue = 0;

            string value = ReadValue(section, key);
            if (!string.IsNullOrEmpty(value))
            {
                int.TryParse(value, out reValue);
            }

            return reValue;
        }
    }
}
